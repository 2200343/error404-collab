var totalQuizScore = [];



function showMultiplechoiceDiv() {
    document.getElementById('multiple-choice').style.display = "block";
    document.getElementById('main').style.display = "none";
    document.getElementById('Matching-type').style.display = "none";
 }

function showMtypeDiv(){
    document.getElementById('Fill-in-the-blank').style.display = "none";
    document.getElementById('Matching-type').style.display = "block";
    document.getElementById('multiple-choice').style.display = "none";
}

function showFillintheblankDiv(){
    document.getElementById('Fill-in-the-blank').style.display = "block";
    document.getElementById('Matching-type').style.display = "none";
}


// authors: Melchiades and Julliard
// This function checks the input of the user in the fill in the blanks question
function submit(){
    let userInput;
    let emptyInput = [];

    for (let i = 1; i <= 10; i++) {
        userInput = localStorage.getItem("idfq" + i);

        if (userInput == null || userInput == "") {
            emptyInput.push(40 + i)
        }
    }

    console.log(emptyInput);

    if (emptyInput === undefined || emptyInput.length == 0) {
        document.getElementById('Fill-in-the-blank').style.display = "none";
        document.getElementById('Submission').style.display = "block";

    } else {
        checkEmptyInputs(emptyInput);
    }
}

// author: Julliard
// The function alerts if the user has an empty input.
function checkEmptyInputs(numbersInput) {
    if(numbersInput.length >= 1) {
        alert(`Please put an answer on number/s: ${numbersInput}`);
    }
}

function showResults(){
    let totalScoreFillIn;
    let totalScoreMultiple;
    let totalScore;
    let score = document.getElementById('p-score');
    document.getElementById('score').style.display = "block";

    totalScoreFillIn = parseInt(localStorage.getItem("correctAnsFill"));
    totalScoreMultiple = parseInt(localStorage.getItem("correctAnsMultiple"));

    totalScore = totalScoreFillIn + totalScoreMultiple;
    score.append(totalScore);
}

function getScore(){
}

