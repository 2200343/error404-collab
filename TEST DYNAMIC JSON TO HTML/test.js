// Get the form and file field
let form = document.querySelector('#upload');
let file = document.querySelector('#file');
var quizJSON;

/**
 * Log the uploaded file to the console
 * @param {event} Event The file loaded event
 */
function logFile (event) {
    let str = event.target.result;
     quizJSON = JSON.parse(str);
    console.log('string', str);
    console.log('json', quizJSON);

}

/**
 * Handle submit eventsquizJSON
 * @param  {Event} event The event object
 */
function handleSubmit (event) {

    // Stop the form from reloading the page
    event.preventDefault();

    // If there's no file, do nothing
    if (!file.value.length) return;

    // Create a new FileReader() object
    let reader = new FileReader();

    // Setup the callback event to run when the file is read
    reader.onload = logFile;

    // Read the file
    reader.readAsText(file.files[0]);

}

// Listen for submit events
form.addEventListener('submit', handleSubmit);


function start() {
    var questionType = document.createElement("h1");
    var questionText = document.createElement("p");
    var answerBox = document.createElement("input");
    var type;

    /*for (var i = current; i < current + 2; i++) {
        var questionType = document.createElement("h1");
        var questionText = document.createElement("p");
        var answerBox = document.createElement("input");

        type = quizJSON[current].type;

        switch (type) {
            case "identification":
                questionType.innerText = "Identification";
                break;
            case "matchingType":
                questionType.innerText = "Matching Type";
                break;
            case "multipleChoice":
                questionType.innerText = "Multiple Choice";
                break;
        }

        questionText.innerText = quizJSON[i].question;
        questionText.style.fontSize = "25px";

        document.body.appendChild(questionType);
        document.body.appendChild(questionText)
        document.body.appendChild(answerBox);
    }*/


    var quizContainer = document.querySelector("#container");
    // IDENTIFICATION FIRST


    /*for (var quizPart in quizJSON)
    {
        for (var i = 0; i < quizPart.length; i++)
        {
            var questionText = document.createElement("p");
            var answerBox = document.createElement("input");
            questionText.innerText = quizPart[i].question;

            quizContainer.append(questionText);
            quizContainer.append(answerBox);
        }
    }*/


    /*for (var i = 0; i < quizJSON.length; i++)
    {
        console.log("Something happened");
        var quizPart = quizJSON[i];
        for (var j = 0; j < quizPart.length; j++)
        {
            var questionText = document.createElement("p");
            var answerBox = document.createElement("input");
            questionText.innerText = quizPart[i].question;

            quizContainer.append(questionText);
            quizContainer.append(answerBox);
        }
    }*/





}





function nextQuizPage()
{
    // first remove the last 3 added elements back then

        var quizContainer = document.querySelector("#container");
        var child;

        for (var i = 0; i < quizJSON.identification.length * 2; i++)
        {
            child = quizContainer.lastElementChild;
            quizContainer.removeChild(child);
        }


    start();
}

