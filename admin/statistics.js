




function login()
{
    var statisticsDiv = document.getElementById("statistics");
    var loginDiv = document.getElementById("login");
    var logoutDiv = document.getElementById("logout");

    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;

    // insert getting data from text file

    if (username == "admin" && password == "admin")
    {
     statisticsDiv.style.display = "block";
     loginDiv.style.display = "none";
     logoutDiv.style.display = "block";
    }

}

function logout()
{
     var statisticsDiv = document.getElementById("statistics");
     var loginDiv = document.getElementById("login");
     var logoutDiv = document.getElementById("logout");

     statisticsDiv.style.display = "none";
     loginDiv.style.display = "block";
     logoutDiv.style.display = "none";
}

// author: Julliard
// This function creates a graph based on the correct answers of the user
function drawBarGraph() {

    let totalScores= [parseInt(localStorage.getItem("correctAnsMultiple")),
                        parseInt(localStorage.getItem("correctAnsFill"))];

    let graphValues = totalScores.splice(',');

    var canvas = document.getElementById("bar-graph");
    var ctx = canvas.getContext("2d");

    var x = 50;
    var width = 40;

    ctx.fillStyle = '#008080';

    for (var i = 0; i < graphValues.length; i++) {
        var h = graphValues[i];
        ctx.fillRect(x, canvas.height - h, width, h);
        x += width + 60;
    }
}

